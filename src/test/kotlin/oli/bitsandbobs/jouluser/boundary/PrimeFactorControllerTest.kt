package oli.bitsandbobs.jouluser.boundary

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

@WebMvcTest
class PrimeFactorControllerTest {
    @Autowired
    private lateinit var mockMvc: MockMvc


    @Test
    fun getPrimeFactors() {
        val get = MockMvcRequestBuilders.get("/primefactors").param("input", 15.toString())
        val result = mockMvc.perform(get).andReturn()
        assertThat(result.response.status).isEqualTo(200)
        assertThat(result.response.contentAsString).isEqualTo("3 5")
    }

    @Test
    fun getBiggerPrimeFactors() {
        val get = MockMvcRequestBuilders.get("/primefactors").param("input", 255.toString())
        val result = mockMvc.perform(get).andReturn()
        assertThat(result.response.status).isEqualTo(200)
        assertThat(result.response.contentAsString).isEqualTo("3 5 17")
    }

    @Test
    fun getEvenBiggerPrimeFactors() {
        val get = MockMvcRequestBuilders.get("/primefactors").param("input", 8051.toString())
        val result = mockMvc.perform(get).andReturn()
        assertThat(result.response.status).isEqualTo(200)
        assertThat(result.response.contentAsString).isEqualTo("83 97")
    }

    @Test
    fun getHonkingPrimeFactors() {
        val get = MockMvcRequestBuilders.get("/primefactors").param("input", 4905437.toString())
        val result = mockMvc.perform(get).andReturn()
        assertThat(result.response.status).isEqualTo(200)
        assertThat(result.response.contentAsString).isEqualTo("29 47 59 61")
    }

    @Test
    fun getHumungousPrimeFactors() {
        val get = MockMvcRequestBuilders.get("/primefactors").param("input", "177099678149")
        val result = mockMvc.perform(get).andReturn()
        assertThat(result.response.status).isEqualTo(200)
        assertThat(result.response.contentAsString).isEqualTo("271013 653473")
    }

    @Test
    fun getBonkahongousPrimeFactors() {
        val get = MockMvcRequestBuilders.get("/primefactors").param("input", "8445097818679")
        val result = mockMvc.perform(get).andReturn()
        assertThat(result.response.status).isEqualTo(200)
        assertThat(result.response.contentAsString).isEqualTo("2526709 3342331")
    }

    @Test
    fun getHungalodonkusPrimeFactors() {
        val get = MockMvcRequestBuilders.get("/primefactors").param("input", "799310944562621")
        val result = mockMvc.perform(get).andReturn()
        assertThat(result.response.status).isEqualTo(200)
        assertThat(result.response.contentAsString).isEqualTo("13567117 58915313")
    }

    @Test
    fun getHungolomghononoloughongousPrimeFactors() {
        val get = MockMvcRequestBuilders.get("/primefactors").param("input", "103487618758254307")
        val result = mockMvc.perform(get).andReturn()
        assertThat(result.response.status).isEqualTo(200)
        assertThat(result.response.contentAsString).isEqualTo("176739781 585536647")
    }
}