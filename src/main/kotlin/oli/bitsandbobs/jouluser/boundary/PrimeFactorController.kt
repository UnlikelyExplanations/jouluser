package oli.bitsandbobs.jouluser.boundary

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import kotlin.math.sqrt

@RestController
class PrimeFactorController {

    @GetMapping("/primefactors")
    fun getFactors(@RequestParam input: String) : String {
        var result = ""

        var number = input.toBigInteger()

        while(number.mod(2.toBigInteger())== 0.toBigInteger()) {
            number /= 2.toBigInteger()
        }

        var runner = 3.toBigInteger()
        while(runner <= number.sqrt()) {
            while (number.mod(runner) == 0.toBigInteger()) {
                result += runner.toString()
                result += " "
                number /= runner;
            }
            runner += 2.toBigInteger()
        }

        if(number > 2.toBigInteger()) {
            result += number.toString()
        }

        return result
    }
}