package oli.bitsandbobs.jouluser

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JouluserApplication

fun main(args: Array<String>) {
	runApplication<JouluserApplication>(*args)
}
